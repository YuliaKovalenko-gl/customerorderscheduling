import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class JobsIterator implements Iterator<List<Job>> {
    private List<Job> jobsStart;
    private List<Job> jobsEnd;
    private List<Job> next;
    private List<Integer> items;
    private boolean first;

    public JobsIterator(List<Job> jobsStart, List<Job> jobsEnd) {
        this.jobsStart = jobsStart;
        this.jobsEnd = jobsEnd;
        this.next = new ArrayList<>(jobsStart);
        this.first = true;
        this.items = new ArrayList<>(jobsStart.size());
        for (int i=0; i<jobsStart.size(); i++) {
            items.add(i);
        }
    }

    public boolean hasNext() {
        return !next.equals(jobsEnd);
    }

    public List<Job> next() {
        if (first) {
            first = false;
            return next;
        }

        int s;
        int j = items.size() - 2;
        while (items.get(j) >= items.get(j + 1)) {
            if (j-- == 0)
                return new ArrayList<>(jobsEnd); // all permutations are considered (iterating ends)
        }

        int k = items.size() - 1;
        while (items.get(j) >= items.get(k)) {
            k--;
        }
        Collections.swap(items, j, k);

        int l = j + 1;
        int r = items.size() - 1; // sort remain part of sequence
        while (l < r) {
            Collections.swap(items, l++, r--);
        }

        next = new ArrayList<>(items.size());
        for (Integer item: items) {
            next.add(jobsStart.get(item));
        }
        return next;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
