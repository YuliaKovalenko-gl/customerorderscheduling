import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class TwoCriteriaCOS {

    public static void main(String[] args) {
        String dirName;
        if (args.length > 0) {
            dirName = args[0];
        } else {
            dirName = ".";
        }
        File[] filesToInput = new File(dirName).listFiles(
                file -> file.getName().matches(".*.csv"));
        if (filesToInput == null) {
            System.out.println("Input files *.csv were not found.\n" +
                    "Usage: java -jar BiCustOrderSch.jar [dirName]");
            return;
        }
        List<File> inputFiles = Arrays.asList(filesToInput);
        File outputFile = new File(dirName.equals(".") ? "res_root.csv" : "res_" + dirName + ".csv");
        constructParetoSets(inputFiles, outputFile);
    }

    public static void constructParetoSets(List<File> inputFiles, File outputFile) {
        long startTime, duration;
        try (PrintWriter pw = new PrintWriter(outputFile)) {
            for (File file : inputFiles) {
                Problem problem = new Problem(file);
                List<Job> jobs = problem.getJobs();
                System.out.println(file.getName());
                System.out.println(problem);
                System.out.println("init jobs: " + jobs);
                System.out.println();

                startTime = System.nanoTime();
                List<Iterator<List<Job>>> jobsIterators = CustomerOrderScheduling.splitJobs(jobs);
                ExecutorService es = Executors.newFixedThreadPool(jobs.size());
                List<Callable<ParetoFlowTimeWeightSum>> tasks = new ArrayList<>();
                for (int i = 0; i < jobs.size(); i++) {
                    tasks.add(new EvaluatingParetoSet(
                            problem,
                            jobsIterators.get(i)));
                }

                List<ParetoFlowTimeWeightSum> paretoSets = new ArrayList<>();
                try {
                    List<Future<ParetoFlowTimeWeightSum>> futures = es.invokeAll(tasks);
                    for (int i = 0; i < jobs.size(); i++) {
                        paretoSets.add(futures.get(i).get());
                        System.out.println(paretoSets.get(i));
                    }
                } catch (InterruptedException | ExecutionException exc) {
                    exc.printStackTrace();
                }
                es.shutdown();

                ParetoFlowTimeWeightSum totalParetoSet =
                        new ParetoFlowTimeWeightSum(paretoSets.get(0).getOptJobs(),
                                paretoSets.get(0).getOptValues());
                for (int i = 1; i < paretoSets.size(); i++) {
                    totalParetoSet.add(paretoSets.get(i));
                }
                System.out.println(totalParetoSet);
                duration = System.nanoTime() - startTime;

                pw.println(file.getName() + ";");
                writeOutputToFile(pw, problem, totalParetoSet, TimeUnit.NANOSECONDS.toMillis(duration));
                pw.println();
            }
        } catch (IOException ex) {
            System.out.println("Output data were not written");
            ex.printStackTrace();
        }
    }

    public static void writeOutputToFile(PrintWriter pw, Problem p,
                                         ParetoFlowTimeWeightSum paretoSet, long timeInMillis)
            throws IOException {
        pw.printf("n;%d;\n", p.getN());
        pw.printf("m;%d;\n", p.getM());

        List<List<Job>> optJobs = paretoSet.getOptJobs();
        List<List<Integer>> optValues = paretoSet.getOptValues();
        pw.printf("jobs N;%d;\n", optJobs.get(0).size());
        pw.printf("time;%d;%d;\n", timeInMillis/60000, (timeInMillis%60000)/1000);
        pw.printf("Pareto set;\n");
        for (int i = 0; i < optJobs.size(); i++) {
            writeList(pw, optValues.get(i));
            pw.printf(";");
            writeList(pw, optJobs.get(i));
            pw.println();
        }
    }

    private static void writeList(PrintWriter pw, List<?> list) {
        pw.printf("%s;", list.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(";")));
    }
}
