import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

public class EvaluatingMinFlowTime implements Callable<MinTotalFlowTime> {
    private static int counter = 0;
    private Problem p;
    private Iterator<List<Job>> jobsIterator;
    private int num = counter++;

    public EvaluatingMinFlowTime(Problem p, Iterator<List<Job>> jobsIterator) {
        this.p = p;
        this.jobsIterator = jobsIterator;
    }

    public MinTotalFlowTime call() {
        int minTotalTime = 0;
        int totalTime;
        List<Job> minJobs = new ArrayList<Job>();
        boolean first = true;
        while (jobsIterator.hasNext()) {
            System.out.println("Thread " + num);
            List<Job> curJobs = jobsIterator.next();
            totalTime = totalFlowTime(p, curJobs);
            if (first || totalTime < minTotalTime) {
                minTotalTime = totalTime;
                minJobs = curJobs;
                first = false;
            }
        }
        return new MinTotalFlowTime(minJobs, minTotalTime);
    }

    private static int totalFlowTime(Problem p, List<Job> jobs) {
        int totalFlowTime = 0;
        for (int i = 0; i < p.getM(); i++) {
            totalFlowTime += flowTimeCustomer(p, i, jobs);
        }
        return totalFlowTime;
    }

    public static int flowTimeCustomer(Problem p, int customer, List<Job> jobs) {
        int flowTime = p.getInitSetupTimes().get(jobs.get(0).getProduct());
        int prevProduct = jobs.get(0).getProduct();
        Job lastJob = Job.getLastJob(jobs, customer);
        for (Job job : jobs) {
            flowTime += job.getTime();
            int product = job.getProduct();
            if (prevProduct != product)
                flowTime += p.getSetupTimes().get(prevProduct).get(product);
            prevProduct = product;

            if (job.equals(lastJob))
                break;
        }
        return flowTime;
    }
}
