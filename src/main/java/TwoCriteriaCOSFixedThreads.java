import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.*;

public class TwoCriteriaCOSFixedThreads {
    private static final int NUM_THREADS = 200;

    public static void main(String[] args) {
        String dirName;
        if (args.length > 0) {
            dirName = args[0];
        } else {
            dirName = ".";
        }
        File[] filesToInput = new File(dirName).listFiles(
                file -> file.getName().matches(".*.csv"));
        if (filesToInput == null) {
            System.out.println("Input files *.csv were not found.\n" +
                    "Usage: java -jar BiCustOrderSch.jar [dirName]");
            return;
        }
        List<File> inputFiles = Arrays.asList(filesToInput);
        File outputFile = new File(dirName.equals(".") ? "res_root.csv" : "res_" + dirName + ".csv");
        constructParetoSets(inputFiles, outputFile);
    }

    public static void constructParetoSets(List<File> inputFiles, File outputFile) {
        long startTime, duration;
        try (PrintWriter pw = new PrintWriter(outputFile)) {
            for (File file : inputFiles) {
                Problem problem = new Problem(file);
                List<Job> jobs = problem.getJobs();
                System.out.println(file.getName());
                System.out.println(problem);
                System.out.println("init jobs: " + jobs);
                System.out.println();
                if (NUM_THREADS/jobs.size() >= jobs.size()-1) {
                    System.out.println("This problem has " + jobs.size() + " jobs. " +
                            "Algorithm is designed for problems with more than " + ((int)Math.sqrt(NUM_THREADS)) + " jobs");
                    pw.printf("%s;\n", file.getName());
                    pw.printf("This problem has %d jobs. " +
                            "Algorithm is designed for problems with more than %d jobs;\n",
                            jobs.size(), ((int)Math.sqrt(NUM_THREADS)));
                    continue;
                }

                startTime = System.nanoTime();
                List<Iterator<List<Job>>> jobsIterators = iteratorsByThreads(jobs);
                ExecutorService es = Executors.newFixedThreadPool(NUM_THREADS);
                List<Callable<ParetoFlowTimeWeightSum>> tasks = new ArrayList<>();
                for (int i = 0; i < NUM_THREADS; i++) {
                    tasks.add(new EvaluatingParetoSet(
                            problem,
                            jobsIterators.get(i)));
                }

                List<ParetoFlowTimeWeightSum> paretoSets = new ArrayList<>();
                try {
                    List<Future<ParetoFlowTimeWeightSum>> futures = es.invokeAll(tasks);
                    for (int i = 0; i < NUM_THREADS; i++) {
                        paretoSets.add(futures.get(i).get());
                        System.out.println(paretoSets.get(i));
                    }
                } catch (InterruptedException | ExecutionException exc) {
                    exc.printStackTrace();
                }
                es.shutdown();

                ParetoFlowTimeWeightSum totalParetoSet =
                        new ParetoFlowTimeWeightSum(paretoSets.get(0).getOptJobs(),
                                paretoSets.get(0).getOptValues());
                for (int i = 1; i < paretoSets.size(); i++) {
                    totalParetoSet.add(paretoSets.get(i));
                }
                System.out.println(totalParetoSet);
                duration = System.nanoTime() - startTime;

                pw.println(file.getName() + ";");
                TwoCriteriaCOS.writeOutputToFile(pw, problem, totalParetoSet, TimeUnit.NANOSECONDS.toMillis(duration));
                pw.println();

            }
        } catch (IOException ex) {
            System.out.println("Output data were not written");
            ex.printStackTrace();
        }
    }

    public static List<Iterator<List<Job>>> iteratorsByThreads(List<Job> init) {
        List<Iterator<List<Job>>> iterators = new ArrayList<>(NUM_THREADS);
        int size = init.size();
        int numThreadsPerGroup = NUM_THREADS/size;
        int remainPerGroup = NUM_THREADS % size;
        List<Integer> threadsPerGroup = new ArrayList<>(size);
        for (int i=0; i<size; i++) {
            if (remainPerGroup-- > 0) {
                threadsPerGroup.add(numThreadsPerGroup + 1);
            } else {
                threadsPerGroup.add(numThreadsPerGroup);
            }
        }
        for (int i=0; i<size; i++) {
            List<Job> initForGroup = new ArrayList<>(init);
            Job jobFirst = initForGroup.remove(i);
            initForGroup.add(0, jobFirst);
            int numJobsPerThread = (size-1)/threadsPerGroup.get(i);
            int remainJobsPerThread = (size-1) % threadsPerGroup.get(i);
            List<Integer> jobsPerThread = new ArrayList<>();
            for (int j=0; j<threadsPerGroup.get(i); j++) {
                if (remainJobsPerThread-- > 0) {
                    jobsPerThread.add(numJobsPerThread+1);
                } else {
                    jobsPerThread.add(numJobsPerThread);
                }
            }
            int startOffset = 1;
            int endOffset = 0;
            for (int j=0; j<threadsPerGroup.get(i); j++) {
                endOffset += jobsPerThread.get(j);
                iterators.add(new JobsIterator(getStartJobs(initForGroup, startOffset),
                        getEndJobs(initForGroup, endOffset)));
                startOffset += jobsPerThread.get(j);
            }
            System.out.println();
        }
        return iterators;
    }

    public static List<Job> getStartJobs(List<Job> init, int offset) {
        List<Job> start = new ArrayList<>(init.size());
        start.add(init.get(0));
        start.add(init.get(offset));
        start.addAll(init.subList(1, offset));
        start.addAll(init.subList(offset+1, init.size()));
        return start;
    }

    public static List<Job> getEndJobs(List<Job> init, int offset) {
        List<Job> end = new ArrayList<>(init.size());
        end.add(init.get(0));
        end.add(init.get(offset));
        List<Job> reverse = new ArrayList<>(init);
        Collections.reverse(reverse);
        int size = init.size();
        end.addAll(reverse.subList(0, size-offset-1));
        end.addAll(reverse.subList(size-offset, size-1));
        return end;
    }

}
