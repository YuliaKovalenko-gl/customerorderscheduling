import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ParetoFlowTimeWeightSum {
    private List<List<Job>> optJobs;
    private List<List<Integer>> optValues;

    public ParetoFlowTimeWeightSum(List<List<Job>> optJobs,
                                   List<List<Integer>> optValues) {
        this.optJobs = optJobs;
        this.optValues = optValues;
    }

    public ParetoFlowTimeWeightSum() {
        this.optJobs = new ArrayList<>();
        this.optValues = new ArrayList<>();
    }

    public List<List<Job>> getOptJobs() {
        return optJobs;
    }

    public List<List<Integer>> getOptValues() {
        return optValues;
    }

    public boolean add(List<Job> jobs, List<Integer> value) {
        if (optValues.isEmpty()) {
            optJobs.add(jobs);
            optValues.add(value);
            return true;
        }

        for (int i=0; i<optJobs.size(); i++) {
            Integer comp = ParetoComparator.compare(optValues.get(i), value);
            if (comp == null) {
                continue;
            }
            if (comp < 0)
                return false;
            optJobs.remove(i);
            optValues.remove(i);
            i--;
        }
        optJobs.add(jobs);
        optValues.add(value);
        return true;
    }

    public void add(ParetoFlowTimeWeightSum paretoSet) {
        for (int i=0; i<paretoSet.optValues.size(); i++) {
            add(paretoSet.optJobs.get(i), paretoSet.optValues.get(i));
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Pareto set\n");
        for (int i=0; i<optJobs.size(); i++) {
            result.append("value = " + optValues.get(i) + "\n");
            result.append(optJobs.get(i) + "\n");
        }
        return result.toString();
    }
}
