import java.util.List;

public class ParetoComparator {

    public static Integer compare(List<? extends Comparable> value1, List<? extends Comparable> value2) {
        int signSum = 0;
        int zeroCount = 0;
        for (int i=0; i<value1.size(); i++) {
            int comp = (int) Math.signum(value1.get(i).compareTo(value2.get(i)));
            if (comp == 0)
                zeroCount++;
            else
                signSum += comp;
        }

        if (zeroCount == value1.size()) {
            return 0;
        }

        if ((signSum-zeroCount) == -1*value1.size()) {
            return -1;
        }
        if ((signSum+zeroCount) == value1.size()) {
            return 1;
        }

        return null; // incomparable
    }
}
