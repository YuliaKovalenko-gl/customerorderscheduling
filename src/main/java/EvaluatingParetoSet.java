import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

public class EvaluatingParetoSet implements Callable<ParetoFlowTimeWeightSum> {
    private static int counter = 0;
    private int num = ++counter;
    private Problem p;
    private Iterator<List<Job>> jobsIterator;
    private int progress = 0;

    public EvaluatingParetoSet(Problem p, Iterator<List<Job>> jobsIterator) {
        this.p = p;
        this.jobsIterator = jobsIterator;
    }

    public ParetoFlowTimeWeightSum call() {
        ParetoFlowTimeWeightSum paretoSet = new ParetoFlowTimeWeightSum();
        ProgressEvaluation progress = null;
        boolean first = true;
        while (jobsIterator.hasNext()) {
            List<Job> curJobs = jobsIterator.next();
            //System.out.println("Thread " + num + ", val = " + value + ", jobs = " + curJobs);
            paretoSet.add(curJobs, valueOfCriteria(curJobs));
            if (first) {
                progress = new ProgressEvaluation(curJobs, num);
                first = false;
            } else {
                progress.updateProgress(curJobs);
            }
        }
        return paretoSet;
    }

    private List<Integer> valueOfCriteria(List<Job> jobs) {
        List<Integer> value = new ArrayList<>(2);
        int totalFlowTime = 0;
        int weightedSum = 0;
        for (int i = 0; i < p.getM(); i++) {
            int flowTime = EvaluatingMinFlowTime.flowTimeCustomer(p, i, jobs);
            totalFlowTime += flowTime;
            if (flowTime <= p.getDueDates().get(i)) {
                weightedSum += p.getWeights().get(i);
            }
        }
        value.add(totalFlowTime);
        value.add((-1)*weightedSum);
        return value;
    }
}

class ProgressEvaluation {
    private final int STEP_PROGRESS = 25;
    private int progress = 0;
    private int size;
    private Job job1;
    private int stepCoeff = 1;
    private int threadNum;

    public ProgressEvaluation(List<Job> jobs, int threadNum) {
        this.job1 = jobs.get(1);
        this.size = jobs.size()-1;
        this.threadNum = threadNum;
    }

    public void updateProgress(List<Job> curJobs) {
        if (!curJobs.get(1).equals(job1)) {
            job1 = curJobs.get(1);
            progress++;
        }
        int percent = progress*100/size;
        if (percent >= STEP_PROGRESS*stepCoeff) {
            System.out.println("Thread " + threadNum + ": " + percent + "% lasted");
            stepCoeff++;
        }
    }


}
