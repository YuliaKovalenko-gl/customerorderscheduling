import java.util.*;
import java.util.concurrent.*;

public class CustomerOrderScheduling {
    public static void evalMinFlowTime() {
        Problem problem = new Problem(3, 3);
        problem.initRandom();
        List<Job> jobs = problem.getJobs();
        System.out.println(problem);
        System.out.println("init jobs: " + jobs);
        System.out.println();

        List<Iterator<List<Job>>> jobsIterators = splitJobs(jobs);
        ExecutorService es = Executors.newFixedThreadPool(jobs.size());
        List<Callable<MinTotalFlowTime>> tasks = new ArrayList<>();
        for (int i = 0; i < jobs.size(); i++) {
            tasks.add(new EvaluatingMinFlowTime(
                    problem,
                    jobsIterators.get(i)));
        }

        List<MinTotalFlowTime> minTotalFlowTimes = new ArrayList<>();
        try {
            List<Future<MinTotalFlowTime>> futures = es.invokeAll(tasks);
            for (int i = 0; i < jobs.size(); i++) {
                minTotalFlowTimes.add(futures.get(i).get());
                System.out.println(minTotalFlowTimes.get(i));
            }
        } catch (InterruptedException | ExecutionException exc) {
            exc.printStackTrace();
        }
        es.shutdown();

        MinTotalFlowTime totalMin = minTotalFlowTimes.stream()
                .min(Comparator.comparingInt(MinTotalFlowTime::getMinValue))
                .get();
        System.out.println(totalMin);
    }

    public static List<Iterator<List<Job>>> splitJobs(List<Job> jobs) {
        List<Iterator<List<Job>>> iterators = new ArrayList<>(jobs.size());
        for (int i = 0; i < jobs.size(); i++) {
            List<Job> jobsStart = new ArrayList<>(jobs);
            Collections.swap(jobsStart, 0, i);
            List<Job> jobsEnd = new ArrayList<>(jobsStart);
            Collections.reverse(jobsEnd);
            Collections.rotate(jobsEnd, 1);
            iterators.add(new JobsIterator(jobsStart, jobsEnd));
        }
        return iterators;
    }

    public static int factorial(int n) {
        if (n == 0)
            return 1;
        int res = 1;
        for (int i = 1; i <= n; i++) {
            res *= i;
        }
        return res;
    }
}


