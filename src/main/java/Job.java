import java.util.List;

public class Job {
    private int customer;
    private int product;
    private int time;

    public Job(int customer, int product, int time) {
        this.customer = customer;
        this.product = product;
        this.time = time;
    }

    public Job() {}

    public int getCustomer() {
        return customer;
    }

    public int getProduct() {
        return product;
    }

    public int getTime() {
        return time;
    }

    public static Job getLastJob(List<Job> jobs, int customer) {
        Job lastJob = jobs.get(0);
        for (Job job: jobs) {
            if (job.customer == customer) {
                lastJob = job;
            }
        }
        return lastJob;
    }

    public String toString() {
        return "(" + (customer+1) + ", " + (product+1) + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Job job = (Job) o;

        if (customer != job.customer) return false;
        if (product != job.product) return false;
        return time == job.time;

    }

    @Override
    public int hashCode() {
        int result = customer;
        result = 31 * result + product;
        result = 31 * result + time;
        return result;
    }
}
