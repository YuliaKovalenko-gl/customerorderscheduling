import java.util.List;

public class MinTotalFlowTime {
    private List<Job> minJobs;
    private Integer minValue;

    public MinTotalFlowTime(List<Job> minJobs, Integer minValue) {
        this.minJobs = minJobs;
        this.minValue = minValue;
    }

    public List<Job> getMinJobs() {
        return minJobs;
    }

    public Integer getMinValue() {
        return minValue;
    }

    @Override
    public String toString() {
        return "min value = " + minValue + "\n" + minJobs;
    }
}
