import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class Problem {
    private final int P_MIN = 10;
    private final int P_MAX = 30;
    private final int S_MIN = 3;
    private final int S_MAX = 10;
    private final int W_MIN = 3;
    private final int W_MAX = 10;

    private int n; //products
    private int m; //customers
    private List<List<Integer>> processingTimes;
    private List<Integer> initSetupTimes;
    private List<List<Integer>> setupTimes;
    private List<Integer> dueDates;
    private List<Integer> weights;

    public Problem(int n, int m) {
        this.n = n;
        this.m = m;
        this.processingTimes = new ArrayList<>(m);
        for (int i=0; i<m; i++) {
            processingTimes.add(new ArrayList<>(n));
        }
        this.initSetupTimes = new ArrayList<>(n);
        this.setupTimes = new ArrayList<>(n);
        for (int i=0; i<n; i++) {
            setupTimes.add(new ArrayList<>(n));
        }
        this.dueDates = new ArrayList<>(m);
        this.weights = new ArrayList<>(m);
    }

    public Problem(File inputFile) {
        readFromFile(inputFile);
    }

    public int getN() {
        return n;
    }

    public int getM() {
        return m;
    }

    public List<List<Integer>> getProcessingTimes() {
        return processingTimes;
    }

    public List<Integer> getInitSetupTimes() {
        return initSetupTimes;
    }

    public List<List<Integer>> getSetupTimes() {
        return setupTimes;
    }

    public List<Integer> getDueDates() {
        return dueDates;
    }

    public List<Integer> getWeights() {
        return weights;
    }

    public void initRandom() {
        initRandProcessingTimes();
        initSetupTimes = generateData(n, S_MAX-S_MIN+1, S_MIN);
        for (int j=0; j<n; j++) {
            List<Integer> times = generateData(n, S_MAX-S_MIN+1, S_MIN);
            setupTimes.set(j, times);
            times.set(j, 0);
        }
        int totalProcTime = processingTimes.stream()
                .map(list -> list.stream().mapToInt(x->x).sum())
                .mapToInt(x->x).sum();
        dueDates = generateData(m, Math.round((n*S_MAX)/2+(totalProcTime)/2)-S_MIN-P_MIN+1, S_MIN+P_MIN);
        weights = generateData(m, W_MAX-W_MIN+1, W_MIN);
    }

    private static List<Integer> generateData(int size, int range, int offset) {
        Random rand = new Random();
        List<Integer> data = new ArrayList<>(size);
        for (int i=0; i<size; i++) {
            data.add(rand.nextInt(range)+offset);
        }
        return data;
    }

    private void initRandProcessingTimes() {
        for (int i=0; i<m; i++) {
            Random rand = new Random();
            int numProducts = rand.nextInt(n)+1;
            List<Integer> processings = new ArrayList<>(n);
            for (int j=0; j<n; j++) {
                processings.add(0);
            }
            Set<Integer> indices = new HashSet<>();
            while (indices.size() != numProducts) {
                indices.add(rand.nextInt(n));
            }
            for (Integer index: indices) {
                processings.set(index, rand.nextInt(P_MAX-P_MIN+1)+P_MIN);
            }
            processingTimes.set(i, processings);
        }
    }

    private void readFromFile(File file) {
        try (Scanner scanner = new Scanner(file).useDelimiter("\\s*;\\s*")) {
            if (scanner.next().equals("n")) {
                n = scanner.nextInt();
            } else {
                throw new IOException();
            }

            if (scanner.next().equals("m")) {
                m = scanner.nextInt();
            } else {
                throw new IOException();
            }

            if (scanner.next().equals("p")) {
                processingTimes = new ArrayList<>(m);
                for (int i=0; i<m; i++) {
                    processingTimes.add(readLineFormFile(scanner, n));
                }
            } else {
                throw new IOException();
            }

            if (scanner.next().equals("init s")) {
                initSetupTimes = readLineFormFile(scanner, n);
            } else {
                throw new IOException();
            }

            if (scanner.next().equals("s")) {
                setupTimes = new ArrayList<>(n);
                for (int i=0; i<n; i++) {
                    setupTimes.add(readLineFormFile(scanner, n));
                }
            } else {
                throw new IOException();
            }

            if (scanner.next().equals("d")) {
                dueDates = readLineFormFile(scanner, m);
            } else {
                throw new IOException();
            }

            if (scanner.next().equals("w")) {
                weights = readLineFormFile(scanner, m);
            } else {
                throw new IOException();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File " + file.getName() + " not found");
        } catch (IOException ex) {
            System.out.println("Invalid input data");
        }
    }

    private List<Integer> readLineFormFile(Scanner sc, int n) {
        List<Integer> result = new ArrayList<>(n);
        for (int i=0; i<n; i++) {
            result.add(sc.nextInt());
        }
        return result;
    }

    public List<Job> getJobs() {
        List<Job> jobs = new ArrayList<>();
        for (int i=0; i<m; i++) {
            for (int j=0; j<n; j++) {
                int time = processingTimes.get(i).get(j);
                if (time > 0) {
                    jobs.add(new Job(i, j, time));
                }
            }
        }
        return jobs;
    }

    public String processingTimes() {
        StringBuilder result =  new StringBuilder("p = \n");
        for (List<Integer> times: processingTimes) {
            result.append(times + "\n");
        }
        return result.toString();
    }

    public String setupTimes() {
        StringBuilder result = new StringBuilder("init s = \n");
        result.append(initSetupTimes + "\n");
        result.append("s = \n");
        for (List<Integer> times: setupTimes) {
            result.append(times + "\n");
        }
        return result.toString();
    }

    public String weights() {
        return "w = " + weights;
    }

    public String dueDates() {
        return "d = " + dueDates;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("n = " + n + "\n");
        result.append("m = " + m + "\n");
        result.append(processingTimes() + "\n");
        result.append(setupTimes() + "\n");
        result.append(dueDates() + "\n");
        result.append(weights() + "\n");
        return result.toString();
    }
}
