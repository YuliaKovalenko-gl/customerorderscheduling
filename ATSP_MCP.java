/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package atsp_mcp;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

class Element {
    private int firstCriterion;
    private int secondCriterion;
    public Element(){
        this.firstCriterion  = 0;
        this.secondCriterion = 0;
     }
     public Element(int firstCriterion, int secondCriterion){
        this.firstCriterion = firstCriterion;
        this.secondCriterion = secondCriterion;
     }

        public int getFirstCriterion() {
            return firstCriterion;
        }

        public int getSecondCriterion() {
            return secondCriterion;
        }

        public void setFirstCriterion(int firstCriterion) {
            this.firstCriterion = firstCriterion;
        }

        public void setSecondCriterion(int secondCriterion) {
            this.secondCriterion = secondCriterion;
        }
     
    }

public class ATSP_MCP {

    /**
     * @param args the command line arguments
     */   
   
    public static void main(String[] args) throws IOException {
        Random rnd = new Random();//датчик случайных чисел
        int N = 20;//число элементов
        int M = 10;//размер подмножества
        int e1Sum = 0, e2Sum = 0;
        int e1min = 1000, e2min = 1000;
        ArrayList<Element> vectors = new ArrayList<Element>();
        //генерируем элементы случайно и добавляем в список
        //Element elem=new Element();
        for (int i = 0; i < N / 4; ++i) {
            Element elem = new Element();
            elem.setFirstCriterion(4);
            e1Sum += elem.getFirstCriterion();
            if(elem.getFirstCriterion()<e1min){
            e1min=elem.getFirstCriterion();
            }

            elem.setSecondCriterion(4);
            e2Sum += elem.getSecondCriterion();
            if(elem.getSecondCriterion()<e2min){
            e2min=elem.getSecondCriterion();
            }
            vectors.add(elem);
            System.out.print(vectors.get(i).getFirstCriterion() + " "
                    + vectors.get(i).getSecondCriterion() + " ;");
        }

        for (int i = N / 4; i < N; ++i) {
            Element elem = new Element();
            elem.setFirstCriterion(rnd.nextInt(5) + 2);//rnd.nextInt(4)+1
            e1Sum += elem.getFirstCriterion();
            if(elem.getFirstCriterion()<e1min){
            e1min=elem.getFirstCriterion();
            }

            elem.setSecondCriterion(rnd.nextInt(4) + 1);//rnd.nextInt(4)+2
            e2Sum += elem.getSecondCriterion();
            vectors.add(elem);
            if(elem.getSecondCriterion()<e2min){
            e2min=elem.getSecondCriterion();
            }

            System.out.print(vectors.get(i).getFirstCriterion() + " "
                    + vectors.get(i).getSecondCriterion() + " ;");
        }
        System.out.println();
        System.out.println("sum: " + e1Sum + " " + e2Sum);
        System.out.println("min: " + e1min + " " + e2min);

        
        //динамическое программирование
       boolean[] xOpt=DP(N, M, e1Sum, e2Sum, vectors);
        //проверка
        int A1best = 0;
        int A2best = 0;
        double Current = 0;
        for (int j = 0; j < N; j++) {
            if (xOpt[j]) {
                System.out.println(j + " " + vectors.get(j).getFirstCriterion() + " " + vectors.get(j).getSecondCriterion());
                Current += ((double) ((M - 1) * (vectors.get(j).getFirstCriterion()) * (vectors.get(j).getFirstCriterion()) - 2 * vectors.get(j).getFirstCriterion() * (A1best)
                        + (M - 1) * (vectors.get(j).getSecondCriterion()) * (vectors.get(j).getSecondCriterion()) - 2 * vectors.get(j).getSecondCriterion() * (A2best))) / M;
                A1best += vectors.get(j).getFirstCriterion();
                A2best += vectors.get(j).getSecondCriterion();
            }
        }
        System.out.println(Current);
        //---------------------FPTAS---------------------------
        ArrayList<Element> vectorsRed = new ArrayList<Element>(); 
        for (int j = 0; j < N; j++) {
           System.out.print(vectors.get(j).getFirstCriterion() + " "
                    + vectors.get(j).getSecondCriterion() + " ;");
        }
        System.out.println();
        e1Sum = 0; e2Sum = 0;
        int e1minRed = 1000, e2minRed = 1000;
        int B=0;
        for (int j = 0; j < N; j++) {
           Element elem = new Element(); 
           elem.setFirstCriterion(vectors.get(j).getFirstCriterion()-e1min);
           e1Sum += elem.getFirstCriterion();
            if(elem.getFirstCriterion()<e1minRed){
            e1minRed=elem.getFirstCriterion();
            }
           elem.setSecondCriterion(vectors.get(j).getSecondCriterion()-e2min);
           e2Sum += elem.getSecondCriterion();
            if(elem.getSecondCriterion()<e2minRed){
            e2minRed=elem.getSecondCriterion();
            }
           vectorsRed.add(elem);
           
           if(elem.getFirstCriterion()>B){
            B=elem.getFirstCriterion();
            }
           if(elem.getSecondCriterion()>B){
            B=elem.getSecondCriterion();
            }
           System.out.print(vectorsRed.get(j).getFirstCriterion() + " "
                    + vectorsRed.get(j).getSecondCriterion() + " ;");
        }
         System.out.println();
        System.out.println("sum: " + e1Sum + " " + e2Sum);
        System.out.println("min: " + e1minRed + " " + e2minRed);   
        System.out.println("B: "+B);
        double epsilon=0.01;
        double K=3*B/((2*2*2*2)*(2*2)*(M*M*M*M)*(1/epsilon+1)*(1/epsilon+1));
        System.out.println("K: "+K);
        xOpt=DP(N, M, e1Sum, e2Sum, vectors);
       //проверка
        A1best = 0;
        A2best = 0;
        Current = 0;
        for (int j = 0; j < N; j++) {
            if (xOpt[j]) {
                System.out.println(j + " " + vectorsRed.get(j).getFirstCriterion() + " " + vectorsRed.get(j).getSecondCriterion());
                Current += ((double) ((M - 1) * (vectorsRed.get(j).getFirstCriterion()) * (vectorsRed.get(j).getFirstCriterion()) - 2 * vectorsRed.get(j).getFirstCriterion() * (A1best)
                        + (M - 1) * (vectorsRed.get(j).getSecondCriterion()) * (vectorsRed.get(j).getSecondCriterion()) - 2 * vectorsRed.get(j).getSecondCriterion() * (A2best))) / M;
                A1best += vectorsRed.get(j).getFirstCriterion();
                A2best += vectorsRed.get(j).getSecondCriterion();
            }
        }
        System.out.println(Current);

    }
    
    public static boolean[] DP(int N, int M, int e1Sum, int e2Sum, ArrayList<Element> vectors) {
        double[][][][] F = new double[N][M][e1Sum + 1][e2Sum + 1];
        boolean[][][][] delta = new boolean[N][M][e1Sum + 1][e2Sum + 1];
        for (int j = 0; j < N; j++) {//по элементам
            for (int m = 0; m < M; ++m) {// по размеру подмножества
                for (int A1 = 0; A1 < e1Sum + 1; ++A1) {//по первой координате
                    for (int A2 = 0; A2 < e2Sum + 1; ++A2) {//по второй координате

                        //если не берем элемент
                        double x0 = -e1Sum*e2Sum*N;//большая константа!
                        if (j - 1 >= 0) {
                            x0 = F[j - 1][m][A1][A2];
                        }
                        //если берем элемент
                        double x1 = -e1Sum*e2Sum*N;//большая константа!
                        if (m == 0) {
                            if (A1 - vectors.get(j).getFirstCriterion() == 0 && A2 - vectors.get(j).getSecondCriterion() == 0) {
                                x1 = ((double) ((M - 1) * (vectors.get(j).getFirstCriterion()) * (vectors.get(j).getFirstCriterion())
                                        + (M - 1) * (vectors.get(j).getSecondCriterion()) * (vectors.get(j).getSecondCriterion()))) / M;
                            }
                        } else {
                            if (j - 1 >= 0 && m - 1 >= 0 && A1 - vectors.get(j).getFirstCriterion() >= 0
                                    && A2 - vectors.get(j).getSecondCriterion() >= 0) {
                                x1 = F[j - 1][m - 1][A1 - vectors.get(j).getFirstCriterion()][A2 - vectors.get(j).getSecondCriterion()];
                                x1 += ((double) ((M - 1) * (vectors.get(j).getFirstCriterion()) * (vectors.get(j).getFirstCriterion()) - 2 * vectors.get(j).getFirstCriterion() * (A1 - vectors.get(j).getFirstCriterion())
                                        + (M - 1) * (vectors.get(j).getSecondCriterion()) * (vectors.get(j).getSecondCriterion()) - 2 * vectors.get(j).getSecondCriterion() * (A2 - vectors.get(j).getSecondCriterion()))) / M;
                            }
                        }
                        //выбираем максимум
                        if (x1 > x0) {
                            F[j][m][A1][A2] = x1;
                            delta[j][m][A1][A2] = true;
                        } else {
                            F[j][m][A1][A2] = x0;
                            delta[j][m][A1][A2] = false;
                        }
//                  System.out.println(j+": "+m+": "+A1+": "+A2+": "
//                          +vectors.get(j).getFirstCriterion()+": "+vectors.get(j).getSecondCriterion()+": "+
//                          F[j][m][A1][A2]+": "+delta[j][m][A1][A2]);
                    }
                }
            }

        }//завершили основные шаги ДП 
   //находим оптимум ЦФ
        double Fbest = 0;
        int A1best = 0, A2best = 0;
        for (int A1 = 0; A1 < e1Sum + 1; ++A1) {//по первой координате
            for (int A2 = 0; A2 < e2Sum + 1; ++A2) {//по второй координате
                if (F[N - 1][M - 1][A1][A2] > Fbest) {
                    Fbest = F[N - 1][M - 1][A1][A2];
                    A1best = A1;
                    A2best = A2;
                }
            }
        }
        System.out.println(M + ": " + Fbest + " " + A1best + " " + A2best);
        //обратный ход
        boolean[] xOpt = new boolean[N];
        int mCurrent = M - 1;
        for (int j = N - 1; j >= 0; j--) {
            if (mCurrent >= 0) {
                if (delta[j][mCurrent][A1best][A2best] == true) {
                    System.out.println((j + 1) + ": " + 1);
                    mCurrent = mCurrent - 1;
                    A1best = A1best - vectors.get(j).getFirstCriterion();
                    A2best = A2best - vectors.get(j).getSecondCriterion();
                    xOpt[j] = true;
                    System.out.println(mCurrent + " " + A1best + " " + A2best + " " + vectors.get(j).getFirstCriterion()
                            + " " + vectors.get(j).getSecondCriterion());
                } else {
                    System.out.println((j + 1) + ": " + 0);
                    System.out.println(mCurrent + " " + A1best + " " + A2best + " " + vectors.get(j).getFirstCriterion()
                            + " " + vectors.get(j).getSecondCriterion());
                    xOpt[j] = false;
                }
            }
        }
        System.out.println(M + ": " + Fbest + " " + A1best + " " + A2best);  
        return xOpt;
     
    }
    
    public static void main2(String[] args) throws IOException {
    FileWriter fileM = new FileWriter("testSched.csv");
    int n=12; // число работ
    System.out.println("n="+n);
    int nCustomer=5;//число заказчиков
    System.out.println("nCustomer="+nCustomer);
    int m=2; // число критериев 
    System.out.println("m="+m);
    Random rnd = new Random();//датчик случайных чисел
    int[] p=new int[n];//длительности работ 
    int pMax=30;
    int pMin=10;
    System.out.print("p=(");
    for(int i=0;i<n-1;++i){
     p[i]=rnd.nextInt(pMax-pMin+1)+pMin;   
     System.out.print(p[i]+", ");
    }
    p[n-1]=rnd.nextInt(pMax-pMin+1)+pMin;   
    System.out.println(p[n-1]+")");
    int [] customerFORjobs=new int[n];
    System.out.print("CforJ=(");
    for(int i=0;i<n-1;++i){
     customerFORjobs[i]=rnd.nextInt(nCustomer)+1;   
     System.out.print(customerFORjobs[i]+", ");
    }
    customerFORjobs[n-1]=rnd.nextInt(nCustomer)+1; 
    System.out.println(customerFORjobs[n-1]+")");
    int[] s=new int[nCustomer];//начальная переналадка на заказ
    int sMax=10;
    int sMin=3;
    System.out.print("s=(");
    for(int i=0;i<nCustomer-1;++i){
     s[i]=rnd.nextInt(sMax-sMin+1)+sMin;
     System.out.print(s[i]+", ");
    }
    s[nCustomer-1]=rnd.nextInt(sMax-sMin+1)+sMin;
    System.out.println(s[nCustomer-1]+")");
    System.out.println("s2=");
    int[][] s2=new int[nCustomer][nCustomer];//переналадки между заказами
        for (int i = 0; i < nCustomer; ++i) {
            for (int j = 0; j < nCustomer; ++j) {
                s2[i][j]=rnd.nextInt(sMax-sMin+1)+sMin;
                if(i==j){s2[i][j]=0;}
                System.out.print(s2[i][j]+" ");
            }
            System.out.println();
        }
    int[] d=new int[nCustomer];//директивные сроки для заказов 
    System.out.print("d=(");
    for(int i=0;i<nCustomer-1;++i){
     d[i]=rnd.nextInt(Math.round((nCustomer*sMax)/2+(n*pMax)/2)-sMin-pMin+1)+sMin+pMin;
     System.out.print(d[i]+", ");
    }
    d[nCustomer-1]=rnd.nextInt(Math.round((nCustomer*sMax)/2+(n*pMax)/2)-sMin-pMin+1)+sMin+pMin;
    System.out.println(d[nCustomer-1]+")");
    
    int[] q=new int[nCustomer];//награда за выполнение заказа в срок
    int qMax=100;
    int qMin=30;
    System.out.print("q=(");
    for(int i=0;i<nCustomer-1;++i){
     q[i]=rnd.nextInt(qMax-qMin+1)+qMin;
     System.out.print(q[i]+", ");
    }
    q[nCustomer-1]=rnd.nextInt(qMax-qMin+1)+qMin;
    System.out.println(q[nCustomer-1]+")");
    int[] pi=new int[n];//перестановка работ 1 2 3 4 ... n
    for(int i=0;i<n;++i){
     pi[i]=i+1;
    }
    int nRes=ParetoSol(n,m,p,s,s2,d,q,customerFORjobs,nCustomer,fileM);
    System.out.println("The Pareto set size = "+nRes);
    // System.out.println("phi1="+phi1(p,s,s2,pi,customerFORjobs,nCustomer));
    // System.out.println("phi1="+phi2(p,s,s2,d,q,pi,customerFORjobs,nCustomer));
    }
    
    //первый критерий (сумма моментов окончания)
    static int phi1(int[] p, int[] s, int[][] s2, int[] pi, int [] customerFORjobs, int nCustomer) {
        int phi1 = 0;
        int[] C=new int[pi.length]; 
        C[0]=s[customerFORjobs[pi[0]-1]-1]+p[pi[0]-1];
//        System.out.println("C("+0+")="+C[0]);
        
        for (int i = 1; i < pi.length; ++i) {
            C[i]=C[i-1]+s2[customerFORjobs[pi[i-1]-1]-1][customerFORjobs[pi[i]-1]-1]+p[pi[i]-1];
//            System.out.println("C("+i+")="+C[i]);            
        }
        int[] Ccustomer=new int[nCustomer]; 
        for (int j = 0; j < nCustomer; ++j) {
            Ccustomer[j] = 0;
            for (int i = 0; i < pi.length; ++i) {
                if (customerFORjobs[pi[i] - 1] - 1 == j) {
                    Ccustomer[j] = C[i];
                }
            }
//            System.out.println("Ccustumer("+(j+1)+")="+Ccustomer[j]);  
            phi1+=Ccustomer[j];
        }
        return phi1;
    }
    
        //второй критерий (сумма наград за своевременное выполнение)
    static int phi2(int[] p, int[] s, int[][] s2, int[]d, int[]q, int[] pi, int [] customerFORjobs, int nCustomer) {
        int phi2 = 0;
        int[] C=new int[pi.length];//моменты окончания работ 
        C[0]=s[customerFORjobs[pi[0]-1]-1]+p[pi[0]-1];
//        System.out.println("C("+0+")="+C[0]);
        
        for (int i = 1; i < pi.length; ++i) {
            C[i]=C[i-1]+s2[customerFORjobs[pi[i-1]-1]-1][customerFORjobs[pi[i]-1]-1]+p[pi[i]-1];
//            System.out.println("C("+i+")="+C[i]);            
        }
        int[] Ccustomer=new int[nCustomer]; //моменты выполнения заказов
        for (int j = 0; j < nCustomer; ++j) {
            Ccustomer[j] = 0;
            for (int i = 0; i < pi.length; ++i) {
                if (customerFORjobs[pi[i] - 1] - 1 == j) {
                    Ccustomer[j] = C[i];
                }
            }
//            System.out.println("Ccustumer("+(j+1)+")="+Ccustomer[j]);  
            if(Ccustomer[j] <= d[j]){
            phi2+=q[j];
            }
        }
         return phi2;
    }
    
    //число недоминируемых точек    
 static int ParetoSol(int n, int m, int[] p, int[] s, int[][]s2, int[] d, int[] q, int [] customerFORjobs, int nCustomer, FileWriter fileM) throws IOException{
     fileM.write("n; "+n+";\n"); 
     fileM.write("m; "+m+";\n"); 
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    int k=1;//число решений (n)!
//    for (int i = 2; i <= n; ++i) {
//      k=k*i;  
//    }
//    System.out.println("k="+k); 
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    int [] b=new int[n];//текущая перестановка
    for (int i = 0; i < n; i++){b[i] = i + 1;}//1 2 3 4 5 6 ...
    for (int i = 0; i < n; i++){System.out.print(b[i]+" ");}
    //System.out.println(); 
    int [] criteria0=new int[m];//значения критериев для текущей перестановки
    criteria0[0]=phi1(p,s,s2,b,customerFORjobs,nCustomer);
    criteria0[1]=-phi2(p,s,s2,d,q,b,customerFORjobs,nCustomer);
    ArrayList listSol=new ArrayList();
    listSol.add(criteria0);
    int[] list_current=(int[])  listSol.get(0);
    System.out.println(list_current[0]+" "+list_current[1]); 
    boolean flagTemp=true;
    while(flagTemp){
    b=NextSet(b,n);
    for (int i = 0; i < n; i++){System.out.print(b[i]+" ");}
    //System.out.println();
    //значения критериев
    int [] criteria=new int[m];//значения критериев для текущей перестановки
    criteria[0]=phi1(p,s,s2,b,customerFORjobs,nCustomer);
    criteria[1]=-phi2(p,s,s2,d,q,b,customerFORjobs,nCustomer);
    System.out.println(criteria[0]+" "+criteria[1]);
    
    boolean flagT; boolean flagAdd=true;
    boolean difT=true; int i1=0;
    while(difT){
    ////////////////////////////////////////////////////////////////////////////
        list_current = (int[]) listSol.get(i1);
        flagT = true;//есть ли доминирование list_current <= criteria?
        if (list_current[0] <= criteria[0] && list_current[1] <= criteria[1]) {
            flagT = flagT & true;
        } else {
            flagT = flagT & false;
        }

        if (flagT == true) {
            flagAdd = false;
            break;
        }//criteria не добавляется в список
        else {
            if ((list_current[0] > criteria[0] && list_current[1] >= criteria[1])
                    || (list_current[0] >= criteria[0] && list_current[1] > criteria[1])) {
                listSol.remove(i1);//criteria доминирует list_current => удаляем list_current из списка
                i1--;
            }
        }
        i1++;
        if (i1 >= listSol.size()) {
            difT = false;
            break;
        }
     ////////////////////////////////////////////////////////////////////////////       
    }
    if(flagAdd==true){listSol.add(criteria);//добавляем criteria в список
    }    
    //проверка критерия остановки перебора перестановок
    int count=0;
    for (int i = 0; i < n; i++){if(b[i]==n-i) count++;}
    if(count==n){break;}
    }
    //The Pareto front
    for(int i=0; i<listSol.size();i++){
    list_current = (int[]) listSol.get(i);
    System.out.println("*[ "+list_current[0]+" "+list_current[1]+" ]*");
    }    
    return listSol.size();
//    //перебор перестановок
//    int [][] valueS=new int[k][m];
//    int [] a=new int[n];//текущая перестановка
//    for (int i = 0; i < n; i++){a[i] = i + 1;}//1 2 3 4 5 6 ...
//    for (int i = 0; i < n; i++){System.out.print(a[i]+" ");}
//    //значения критериев
//    valueS[0][0]=phi1(p,s,s2,a,customerFORjobs,nCustomer);
//    valueS[0][1]=-phi2(p,s,s2,d,q,a,customerFORjobs,nCustomer);
//    System.out.println(); 
//    for(int j=1;j<k;j++){
//    a=NextSet(a,n);
//    //for (int i = 0; i < n; i++){System.out.print(a[i]+" ");}
//    //значения критериев
//    valueS[j][0]=phi1(p,s,s2,a,customerFORjobs,nCustomer);
//    valueS[j][1]=-phi2(p,s,s2,d,q,a,customerFORjobs,nCustomer);
//    //System.out.println();
//    }
//    
////         for(int j=0;j<k;j++){
////             System.out.print("[ ");
////             for (int i = 0; i < m; i++){
////             System.out.print(valueS[j][i]+" ");}
////              System.out.println("]");
////         }
//      //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
//    boolean[] flagS=new boolean[k];
//    boolean flag; short dif;
//    int jprev, iprev;
//    int i=0;
//    int j=1;
//       
//     while(i!=-1 && j!=-1){// строим фронт Парето
////        System.out.println(i+" "+j); 
//        flag = true;//есть ли доминирование i > j?
//        dif = 0;
//        for (int l = 0; l < m; l++) {
//            if (valueS[i][l] <= valueS[j][l]) {
//                flag = flag & true;
//                dif = (short) (dif + valueS[j][l] - valueS[i][l]);
//            } else {
//                flag = flag & false;
//                break;
//            }
//        }
//    
//        if (flag == true) {//доминирование есть %dif > 0 &&% исключаем из рассмотрения одинаковые решения
//            //для решений с одинаковыми значениями по всем критериям оставляем один экземпляр
//            flagS[j] = true;//удаляем j
//            jprev = j;
//            j=next(j,k,flagS);//переходим к следующему j
//            if (jprev == j) {
//                iprev = i;
//                i=next(i,k,flagS);//переходим к следующему i
//                if(iprev == i) {i = -1;} else{
//                    jprev = j = i;
//                    j=next(i,k,flagS);//переходим к следующему j
//                    if (jprev == j) {j = -1;}
//                }
//            }
//        }else{//доминирования нет
//        flag = true;//есть ли доминирование j > i?
//        dif = 0;
//        for (int l = 0; l < m; l++) {
//            if (valueS[j][l] <= valueS[i][l]) {
//                flag = flag & true;
//                dif = (short) (dif + valueS[i][l] - valueS[j][l]);
//            } else {
//                flag = flag & false;
//                break;
//            }
//        }
//        if (dif > 0 && flag == true) {//доминирование есть
//         flagS[i] = true;//удаляем i
//         iprev = i;
//         i=next(i,k,flagS);//переходим к следующему i
//         if(iprev == i) {i = -1;} else{
//         jprev = j = i;
//         j=next(i,k,flagS);//переходим к следующему j
//         if (jprev == j) {j = -1;}}
//        }else{
//         jprev = j;
//         j=next(j,k,flagS);//переходим к следующему j
//         if (jprev == j) {
//         iprev = i;
//         i=next(i,k,flagS);//переходим к следующему i
//         if(iprev == i) {i = -1;} else{
//         jprev = j = i;
//         j=next(i,k,flagS);//переходим к следующему j
//         if (jprev == j) {j = -1;}}}}//else
//        }//else
//     }//while
//     i=0;
//     fileM.write("Pareto Set"+";\n");
//        for (int l = 0; l < k; l++){
//        if(flagS[l]==false){
//            i++;//считаем число недоминируемых решений
//         System.out.print("[ ");
//         for (int t = 0; t < m; t++){
//             System.out.print(valueS[l][t]+" ");
//             fileM.write(valueS[l][t]+";");}
//              System.out.println("]");
//              fileM.write(";\n");
//        }
//        } 
//        return i;
 }   
     static int next(int j, int k, boolean[] flagS) {//поиск следующего активного элемента
        int jnext = j;
        for (int i = j + 1; i < k; ++i) {
            if (flagS[i] == false) {
                jnext = i;
                break;
            }
        }
        return jnext;
    }
    //переход к следующей перестановке   
    static int[] NextSet(int[] a, int n) {
        int s;
        int j = n - 2;
        while (j != -1 && a[j] >= a[j + 1]) {
            j--;
        }
        if (j == -1) {
            return a; // больше перестановок нет
        }
        int k = n - 1;
        while (a[j] >= a[k]) {
            k--;
        }
        s = a[j];
        a[j] = a[k];
        a[k] = s;
        int l = j + 1, r = n - 1; // сортируем оставшуюся часть последовательности
        while (l < r) {
            s = a[l];
            a[l] = a[r];
            a[r] = s;
            l++;
            r--;
        }
        return a;
    }
    public static void main1(String[] args) throws IOException {
    FileWriter fileM = new FileWriter("Example_MTSP_m2_n20_N50_12contr.csv");
    Scanner scanner = new Scanner(new FileInputStream("Example_MTSP_m2_n20_N50_1_20.txt"));//входные данные
    int n; // число вершин
    int m=2; // число критериев
    short w11;//первый критерий важнее второго (w11 - убыло по первому критерию, w21- прибыло по второму критерию)
    short w21;//первый критерий важнее второго (-w11,w21)>(0,0) - предпочтительнее
    short w12;//второй критерий важнее первого (w12 - прибыло по первому критерию, w22 - убыло по второму критерию)
    short w22;//второй критерий важнее первого (w12,-w22)>(0,0) - предпочтительнее
    //согласно критерию непротиворечивости w11*w22-w12*w21>0
    boolean isRand=true;
    short[] maxS=new short[m]; //int maxElem=2;
    short[] minS=new short[m]; //int minElem=1;
    Random rnd = new Random();//датчик случайных чисел
    int exampleN;//число примеров
    int number;
    long startTime;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //инициализация длин дуг (считываем из файла)
    short [][][] listS;
    System.out.println(scanner.nextLine());
    exampleN = scanner.nextInt();
    System.out.println(exampleN);

    for(int i_ex=0;i_ex<exampleN;++i_ex){
        System.out.println(scanner.nextLine());
        System.out.println(scanner.nextLine()); 
        fileM.write(scanner.nextLine()+";\n");//название задачи
        System.out.println(scanner.nextLine()); System.out.println(scanner.nextLine()); 
        n = scanner.nextInt(); 
        System.out.println(n);//число вершин        
        System.out.println(scanner.nextLine()); System.out.println(scanner.nextLine()); 
        System.out.println(scanner.nextLine()); 
        listS=new short[m][n][n];
        for (int l = 0; l < n; ++l) {//длины дуг
            for (int j = 0; j < n; ++j) {
                listS[0][l][j] = (short) (scanner.nextInt());
                System.out.print(listS[0][l][j] + " ");
                if (l == 0 && j == 1) {
                    maxS[0] = listS[0][l][j];
                    minS[0] = listS[0][l][j];
                }
                if (l != j) {
                    if (maxS[0] < listS[0][l][j]) {
                        maxS[0] = listS[0][l][j];
                    }
                    if (minS[0] > listS[0][l][j]) {
                        minS[0] = listS[0][l][j];
                    }
                }
            }
            System.out.println();
        }
     System.out.println(scanner.nextLine()); System.out.println(scanner.nextLine()); 
     System.out.println(scanner.nextLine());
        for (int l = 0; l < n; ++l) {
            for (int j = 0; j < n; ++j) {
                listS[1][l][j] = (short) (scanner.nextInt());
                System.out.print(listS[1][l][j] + " ");
                if (l == 0 && j == 1) {
                    maxS[1] = listS[1][l][j];
                    minS[1] = listS[1][l][j];
                }
                if (l != j) {
                    if (maxS[1] < listS[1][l][j]) {
                        maxS[1] = listS[1][l][j];
                    }
                    if (minS[1] > listS[1][l][j]) {
                        minS[1] = listS[1][l][j];
                    }
                }
            }
            System.out.println();
        }
//    if(isRand==true){
//        for (int i = 0; i < m; ++i) {
//            maxS[i]=0; minS[i]=(short)(minElem+maxElem);//обе матрицы генерируются случайно
////        System.out.println("S["+(i+1)+"]");    
//            for (int l = 0; l < n; ++l) {
//                for (int j = 0; j < n; ++j) {
//                    if (l != j) {
//                        listS[i][l][j] = (short) (rnd.nextInt(maxElem) + minElem);
//                        if(maxS[i]<listS[i][l][j]) {maxS[i]=listS[i][l][j];}
//                        if(minS[i]>listS[i][l][j]) {minS[i]=listS[i][l][j];}
//                    } else {
//                        listS[i][l][j] = 0;
//                    }
////                    System.out.print(listS[i][l][j]);
//                }
////                System.out.println();
//            }
////           System.out.println(); 
//        }
//    }//if (isRand
//    else{
//
//        maxS[0]=0; minS[0]=(short)(minElem+maxElem);//первая генерируется случайно f1
////        System.out.println("S["+1+"]");    
//            for (int l = 0; l < n; ++l) {
//                for (int j = 0; j < n; ++j) {
//                    if (l != j) {
//                        listS[0][l][j] = (short) (rnd.nextInt(maxElem) + minElem);
//                        if(maxS[0]<listS[0][l][j]) {maxS[0]=listS[0][l][j];}
//                        if(minS[0]>listS[0][l][j]) {minS[0]=listS[0][l][j];}
//                    } else {
//                        listS[0][l][j] = 0;
//                    }
////                    System.out.print(listS[0][l][j]);
//                }
////                System.out.println();
//            }
////        System.out.println("S["+2+"]"); 
//         maxS[1]=0; minS[1]=(short)(maxS[0]+1);//f2=max+1-f1 противоположные цели
//         // при w2>=w1 сужение до одной точки
//         // при w2<w1 сужения нет вообще
//            for (int l = 0; l < n; ++l) {
//                for (int j = 0; j < n; ++j) {
//                    if (l != j) {
//                        listS[1][l][j] = (short)(maxS[0]+1-listS[0][l][j]);
//                        if(maxS[1]<listS[1][l][j]) {maxS[1]=listS[1][l][j];}
//                        if(minS[1]>listS[1][l][j]) {minS[1]=listS[1][l][j];}
//                    } else {
//                        listS[1][l][j] = 0;
//                    }
////                    System.out.print(listS[1][l][j]);
//                }
////                System.out.println();
//            }
//
//
//    }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////                                
                                             //множество Парето исходной задачи//      
    n=10;//!!!!убрать!!!!
     for (int i = 0; i < m; ++i) {
    System.out.println("l["+(i+1)+"]="+((maxS[i]-minS[i])*n)+"; max "+maxS[i]+"; min "+minS[i]);
    fileM.write("l["+(i+1)+"];"+((maxS[i]-minS[i])*n)+"; max ;"+maxS[i]+"; min ;"+minS[i]+";\n");
    }
    startTime = System.currentTimeMillis();//засекаем время начала выполнения 
    number=numberParetoSol(n,m,listS,fileM);//строим фронт Парето
    fileM.write("N;"+number+";\n");
    fileM.write("Time;"+(System.currentTimeMillis()-startTime)+";millisec.;"+";\n");
    System.out.println("N="+number); 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////    
  //////////////////////////////////////////////////////////////////////////////////////////////////////////                                
                                             //сужение множества Парето//  
   //первый критерий важнее второго (первый критерий без изменений f1, а второй меняем f2=w11*f2+w21*f1)//
        w11=2;//первый критерий важнее второго (w11 - убыло по первому критерию, w21- прибыло по второму критерию)
        w21=1;//первый критерий важнее второго (-w11,w21)>(0,0) - предпочтительнее
        fileM.write("Contracted Pareto Set"+";\n");
        fileM.write("w1;"+w11+";\n");
        fileM.write("w2;"+w21+";\n");
        minS[1]=(short)((maxS[1]+ maxS[0])*(w11+w21));
        maxS[1]=0; 
        for (int l = 0; l < n; ++l) {
                for (int j = 0; j < n; ++j) {
               listS[1][l][j] = (short)(w11*listS[1][l][j]+w21*listS[0][l][j]);
               if(maxS[1]<listS[1][l][j]) {maxS[1]=listS[1][l][j];}
               if(listS[1][l][j]>0 && minS[1]>listS[1][l][j]) {minS[1]=listS[1][l][j];}
                }
         }
     for (int i = 0; i < m; ++i) {
     System.out.println("l["+(i+1)+"]="+((maxS[i]-minS[i])*n)+"; max "+maxS[i]+"; min "+minS[i]);
     fileM.write("l["+(i+1)+"];"+((maxS[i]-minS[i])*n)+"; max ;"+maxS[i]+"; min ;"+minS[i]+";\n");}      
     startTime = System.currentTimeMillis();//засекаем время начала выполнения 
     number=numberParetoSol(n,m,listS,fileM);//строим фронт Парето
     fileM.write("N;"+number+";\n");
     fileM.write("Time;"+(System.currentTimeMillis()-startTime)+";millisec.;"+";\n");
     System.out.println("N="+number);  
     
        for (int l = 0; l < n; ++l) {//возвращаем все обратно
                for (int j = 0; j < n; ++j) {
               listS[1][l][j] = (short)((listS[1][l][j]- w21*listS[0][l][j])/w11);
                }
        }   
        w11=1;//первый критерий важнее второго (w11 - убыло по первому критерию, w21- прибыло по второму критерию)
        w21=2;//первый критерий важнее второго (-w11,w21)>(0,0) - предпочтительнее
        fileM.write("Contracted Pareto Set"+";\n");
        fileM.write("w1;"+w11+";\n");
        fileM.write("w2;"+w21+";\n");
        minS[1]=(short)((maxS[1]+ maxS[0])*(w11+w21));
        maxS[1]=0; 
        for (int l = 0; l < n; ++l) {
                for (int j = 0; j < n; ++j) {
               listS[1][l][j] = (short)(w11*listS[1][l][j]+w21*listS[0][l][j]);
               if(maxS[1]<listS[1][l][j]) {maxS[1]=listS[1][l][j];}
               if(listS[1][l][j]>0 && minS[1]>listS[1][l][j]) {minS[1]=listS[1][l][j];}
                }
         }
     for (int i = 0; i < m; ++i) {
     System.out.println("l["+(i+1)+"]="+((maxS[i]-minS[i])*n)+"; max "+maxS[i]+"; min "+minS[i]);
     fileM.write("l["+(i+1)+"];"+((maxS[i]-minS[i])*n)+"; max ;"+maxS[i]+"; min ;"+minS[i]+";\n");}      
     startTime = System.currentTimeMillis();//засекаем время начала выполнения 
     number=numberParetoSol(n,m,listS,fileM);//строим фронт Парето
     fileM.write("N;"+number+";\n");
     fileM.write("Time;"+(System.currentTimeMillis()-startTime)+";millisec.;"+";\n"); 
     System.out.println("N="+number);
  //////////////////////////////////////////////////////////////////////////////////////////////////////////                                
                                             //сужение множества Парето//       
//////////////////////////////////////////////////////////////////////////////////////////////////////////  
    }
     fileM.close();scanner.close();
    }
    
  

 //вычисление длины маршрута   
    static short phitness(short[][] s, int[] p) {
        short phi_c = 0;
        for (int i = 0; i < p.length - 1; ++i) {
            phi_c = (short) (phi_c + s[p[i] - 1][p[i + 1] - 1]);
        }
        phi_c = (short) (phi_c + s[p[p.length - 1] - 1][p[0] - 1]);
        return phi_c;
    }
    
//число недоминируемых точек    
 static int numberParetoSol(int n, int m, short[][][] listS, FileWriter fileM) throws IOException{
     fileM.write("n; "+n+";\n"); 
     fileM.write("m; "+m+";\n"); 
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    int k=1;//число решений (n-1)!
    for (int i = 2; i < n; ++i) {
      k=k*i;  
    }
    System.out.println("k="+k); 
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //перебор перестановок
    short [][] valueS=new short[k][m];
    int [] a=new int[n];//текущая перестановка
    for (int i = 0; i < n; i++){a[i] = i + 1;}//1 2 3 4 5 6 ...
//    for (int i = 0; i < n; i++){System.out.print(a[i]+" ");}
    for (int j = 0; j < m; j++){valueS[0][j]=phitness(listS[j], a);}//значения критериев
//    System.out.println(); 
    for(int j=1;j<k;j++){
    a=NextSet(a,n);
//    for (int i = 0; i < n; i++){System.out.print(a[i]+" ");}
    for (int i = 0; i < m; i++){valueS[j][i]=phitness(listS[i], a);}
//    System.out.println();
    }
    
//         for(int j=0;j<k;j++){
//             System.out.print("[ ");
//             for (int i = 0; i < m; i++){
//             System.out.print(valueS[j][i]+" ");}
//              System.out.println("]");
//         }
      //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
    boolean[] flagS=new boolean[k];
    boolean flag; short dif;
    int jprev, iprev;
    int i=0;
    int j=1;
       
     while(i!=-1 && j!=-1){// строим фронт Парето
//        System.out.println(i+" "+j); 
        flag = true;//есть ли доминирование i > j?
        dif = 0;
        for (int l = 0; l < m; l++) {
            if (valueS[i][l] <= valueS[j][l]) {
                flag = flag & true;
                dif = (short) (dif + valueS[j][l] - valueS[i][l]);
            } else {
                flag = flag & false;
                break;
            }
        }
    
        if (flag == true) {//доминирование есть %dif > 0 &&% исключаем из рассмотрения одинаковые решения
            //для решений с одинаковыми значениями по всем критериям оставляем один экземпляр
            flagS[j] = true;//удаляем j
            jprev = j;
            j=next(j,k,flagS);//переходим к следующему j
            if (jprev == j) {
                iprev = i;
                i=next(i,k,flagS);//переходим к следующему i
                if(iprev == i) {i = -1;} else{
                    jprev = j = i;
                    j=next(i,k,flagS);//переходим к следующему j
                    if (jprev == j) {j = -1;}
                }
            }
        }else{//доминирования нет
        flag = true;//есть ли доминирование j > i?
        dif = 0;
        for (int l = 0; l < m; l++) {
            if (valueS[j][l] <= valueS[i][l]) {
                flag = flag & true;
                dif = (short) (dif + valueS[i][l] - valueS[j][l]);
            } else {
                flag = flag & false;
                break;
            }
        }
        if (dif > 0 && flag == true) {//доминирование есть
         flagS[i] = true;//удаляем i
         iprev = i;
         i=next(i,k,flagS);//переходим к следующему i
         if(iprev == i) {i = -1;} else{
         jprev = j = i;
         j=next(i,k,flagS);//переходим к следующему j
         if (jprev == j) {j = -1;}}
        }else{
         jprev = j;
         j=next(j,k,flagS);//переходим к следующему j
         if (jprev == j) {
         iprev = i;
         i=next(i,k,flagS);//переходим к следующему i
         if(iprev == i) {i = -1;} else{
         jprev = j = i;
         j=next(i,k,flagS);//переходим к следующему j
         if (jprev == j) {j = -1;}}}}//else
        }//else
     }//while
     i=0;
     fileM.write("Pareto Set"+";\n");
        for (int l = 0; l < k; l++){
        if(flagS[l]==false){
            i++;//считаем число недоминируемых решений
         System.out.print("[ ");
         for (int t = 0; t < m; t++){
             System.out.print(valueS[l][t]+" ");
             fileM.write(valueS[l][t]+";");}
              System.out.println("]");
              fileM.write(";\n");
        }
        } 
        return i;
 }   
    
}
